import { describe, expect, test } from "vitest";
import { mount } from "@vue/test-utils";
import MthUploadFile from "../MthUploadFile.vue";
import { createVuetify } from "vuetify";

function uploadFileWrapper(
  validateFormat: (arg0: File[]) => boolean,
  columnNames?: string[],
  chooseColumnNames?: boolean,
) {
  const vuetify = createVuetify();
  return mount(MthUploadFile, {
    global: {
      plugins: [vuetify],
    },
    props: {
      validateFormat,
      columnNames,
      chooseColumnNames,
    },
  });
}

describe("MthUploadFile", () => {
  test("should render the component", () => {
    const wrapper = uploadFileWrapper(() => true);

    // checkbox for first line as header
    expect(wrapper.find("input[type='checkbox']").exists()).toBe(true);
    // type of sepearator
    expect(wrapper.find("input[type='radio']").exists()).toBe(true);
    // input file
    expect(wrapper.find("input[type='file']").exists()).toBe(true);
  });
  test("should render the component with column names", () => {
    const wrapper = uploadFileWrapper(() => true, ["hello", "world"], true);

    // vselect for column names
    expect(wrapper.find(".v-select").exists()).toBe(true);
    // buttons to add
    expect(wrapper.find("[data-testId='addButton']").exists()).toBe(true);
  });
  test("OK button should be disabled", () => {
    const wrapper = uploadFileWrapper(() => true);

    // button to validate
    expect(
      (
        wrapper.find("[data-testId='submitButton']")
          .element as HTMLButtonElement
      ).disabled,
    ).toBeTruthy();
  });
  test("clicking on the buttons should add and delete columns", async () => {
    const wrapper = uploadFileWrapper(() => true, ["hello", "world"], true);

    // add button
    await wrapper.find("[data-testId='addButton']").trigger("click");
    expect(wrapper.findAll(".v-select").length).toEqual(2);

    // delete button
    await wrapper.find("[data-testId='removeButton']").trigger("click");
    expect(wrapper.findAll(".v-select").length).toEqual(1);
    // button should no be available
    expect(wrapper.find("[data-testId='removeButton']").exists()).toBe(false);
  });
});
