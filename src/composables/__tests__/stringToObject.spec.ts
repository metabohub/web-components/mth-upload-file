import { describe, expect, test } from "vitest";
import { stringToObject } from "../Utils";

const filesExample = {
  csv: "metab_1,prot_1\nmetab_2,prot_2",
  scsv: "metab_1;prot_1\nmetab_2;prot_2",
  tsv: "metab_1\tprot_1\nmetab_2\tprot_2",
  withHeader: "metabolites,proteins\nmetab_1,prot_1\nmetab_2,prot_2",
};

describe("stringToObject", () => {
  test("should parse the comma separated file", () => {
    const result = stringToObject(filesExample.csv, ",", false);

    expect(result).toEqual({
      0: ["metab_1", "metab_2"],
      1: ["prot_1", "prot_2"],
    });
  });
  test("should parse the semi colon separated file", () => {
    const result = stringToObject(filesExample.scsv, ";", false);

    expect(result).toEqual({
      0: ["metab_1", "metab_2"],
      1: ["prot_1", "prot_2"],
    });
  });
  test("should parse the tabulation separated file", () => {
    const result = stringToObject(filesExample.tsv, "\t", false);

    expect(result).toEqual({
      0: ["metab_1", "metab_2"],
      1: ["prot_1", "prot_2"],
    });
  });
  test("should parse the file with a header", () => {
    const result = stringToObject(filesExample.withHeader, ",", true);

    expect(result).toEqual({
      metabolites: ["metab_1", "metab_2"],
      proteins: ["prot_1", "prot_2"],
    });
  });
  test("should parse the file with new keys", () => {
    const result = stringToObject(filesExample.csv, ",", false, [
      "hello",
      "world",
    ]);

    expect(result).toEqual({
      hello: ["metab_1", "metab_2"],
      world: ["prot_1", "prot_2"],
    });
  });
  test("more keys than column : should return undefined", () => {
    const result = stringToObject(filesExample.csv, ",", false, [
      "hello",
      "world",
      "!",
    ]);

    expect(result).toEqual({
      hello: ["metab_1", "metab_2"],
      world: ["prot_1", "prot_2"],
      "!": [undefined, undefined],
    });
  });
  test("less keys than columns : should ignore the extra columns", () => {
    const result = stringToObject(filesExample.csv, ",", false, ["hello"]);

    expect(result).toEqual({
      hello: ["metab_1", "metab_2"],
    });
  });
  test("a key is null : should ignore the column", () => {
    const result = stringToObject(filesExample.csv, ",", false, [
      null,
      "world",
    ]);

    expect(result).toEqual({
      world: ["prot_1", "prot_2"],
    });
  });
});
