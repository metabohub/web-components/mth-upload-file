export function stringToObject(
  str: string,
  separator: string,
  firstLineAsHeader: boolean,
  newKeys?: string[],
): { [key: string]: string[] } {
  const obj: { [key: string]: string[] } = {};

  let ligns: string[] = str.split("\n");
  // remove empty ligns
  ligns = ligns.filter((l) => l != "");
  // init
  let separatedLigns: string[] = ligns[0].split(separator);
  // init object keys and fill object
  if (newKeys != undefined) {
    // keys will be the arg 'newKeys'
    newKeys.forEach((k) => {
      if (k != null) {
        obj[k] = [];
      }
    });
    if (firstLineAsHeader) {
      // remove first lign
      ligns.shift();
    }
    // fill object
    ligns.forEach((l: string) => {
      separatedLigns = l.split(separator);

      newKeys.forEach((key: string | null, i: number) => {
        if (key != null) {
          obj[key].push(separatedLigns[i]);
        }
      });
    });
  } else {
    if (firstLineAsHeader) {
      // first line will be object keys
      separatedLigns.forEach((c) => {
        obj[c] = [];
      });
      // remove first lign
      ligns.shift();
    } else {
      // object keys will be 0, 1, ...
      separatedLigns.forEach((c: string, i: number) => {
        obj[i.toString()] = [];
      });
    }
    // fill object
    ligns.forEach((l: string) => {
      separatedLigns = l.split(separator);

      Object.keys(obj).forEach((key: string, i: number) => {
        obj[key].push(separatedLigns[i]);
      });
    });
  }
  return obj;
}
