import type { Meta, StoryObj } from "@storybook/vue3";
import { action } from "@storybook/addon-actions";
import MthUploadFile from "@/components/MthUploadFile.vue";

const meta: Meta<typeof MthUploadFile> = {
  title: "MthUploadFile",
  component: MthUploadFile,
  tags: ["autodocs"],
};

export default meta;
type Story = StoryObj<typeof MthUploadFile>;

const validateFormat = (arg0: File[]) => {
  return arg0 != undefined;
};
const columnNames: string[] = ["identifier", "name"];
const chooseColumnNames: boolean = true;

export const Default: Story = {
  render: (args) => ({
    components: { MthUploadFile },
    setup() {
      return {
        ...args,
        loadFile: action("loadFile"),
      };
    },
    template:
      "<MthUploadFile :validateFormat :columnNames :chooseColumnNames @loadFile='loadFile' />",
  }),
  args: {
    validateFormat,
    columnNames,
    chooseColumnNames,
  },
};
