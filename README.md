# mth-upload-file

[![pipeline status](https://forgemia.inra.fr/metabohub/web-components/mth-upload-file/badges/main/pipeline.svg)](https://forgemia.inra.fr/metabohub/web-components/mth-upload-file/-/commits/main)
[![npm package](https://img.shields.io/gitlab/v/tag/metabohub%2Fweb-components%2Fmth-upload-file?gitlab_url=https%3A%2F%2Fforgemia.inra.fr%2F)](https://forgemia.inra.fr/metabohub/web-components/mth-upload-file/-/packages)

File upload input that returns the content of the file in an object.
You can choose if the first line of the file a=is a header and the separator (',', ';' or tabulation).

## Props

| Name        | Description                                             | Type     | Default |
| ----------- | ------------------------------------------------------- | -------- | ------- |
| `validateFormat` | function that checks the format of the file | `(File[]) => boolean` | —       |
| `columnNames` | the available column names | `string[]` | —       |
| `chooseColumnNames?` | whether or not the columns names are rewritable | `boolean` | `false`      |

## Events

| Name     | Description                                 | Type of `$event`                                                    |
| -------- | ------------------------------------------- | ------------------------------------------------------------- |
| `loadFile` | triggered when the file is loaded. Return its content as an object | `{ [key: string]: string[] }` |

## Use example

```html
<template>
  <MthUploadFile :validateFormat :columnNames @loadFile="console.log($event)" />
</template>

<script setup lang="ts">
import { MthUploadFile } from "@metabohub/mth-upload-file"; //import component
import "@metabohub/mth-upload-file/dist/style.css"; // import style
</script>
```

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

Check the coverage of the unit tests :
```sh
npm run coverage
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

### View documentation

```sh
npm run storybook
```

## CICD pipeline

### Deploy

```yml
.publish:
  stage: deploy
  before_script:
    - apt-get update && apt-get install -y git default-jre
    - npm install
    - npm run build
```

This builds the component as an npm package.

